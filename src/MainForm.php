<?php

namespace Drupal\cheat;

use Drupal\Core\Form\FormBase;
use Drupal\cheat\CheatPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MainForm extends FormBase {
  /**
   * @var \Drupal\cheat\Plugin\CheatPluginManager
   */
  protected $cheatPluginManager;
  
  /**
   * @param \Drupal\cheat\Plugin\CheatPluginManager $cheatPluginManager
   */
  public function __construct(CheatPluginManager $cheat_plugin_manager) {
    $this->cheatPluginManager = $cheat_plugin_manager;
  }
  
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.cheat')
    );
  }
  
  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'cheat_main_form';
  }
  
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, array &$form_state) {
    $proxy_count = count(cheat_get_proxy_list('public://cheat-proxy.txt'));
    $form['list_type'] = array(
      '#type' => 'radios',
      '#title' => 'Тип списка прокси адресов',
      '#options' => array(
        'none' => 'Нет (тестовый режим)',
        'text' => 'Текст',
        'prev' => 'Список из прошлого запуска (' . $proxy_count . ')',
        'work' => 'Проверенные и подходящие прокси (' . count(cheat_get_proxy_list('public://cheat-good-proxy.txt')) . ')',
      ),
      '#default_value' => $proxy_count ? 'prev' : 'text',
    );
    if ($proxy_count == 0) {
      unset($form['list_type']['#options']['prev']);
    }
    
    $form['list'] = array(
      '#type' => 'textarea',
      '#title' => 'Список прокси',
      '#description' => 'В формате <code>ip:port</code>',
      '#rows' => 10,
      '#attributes' => array('autofocus' => TRUE),
      '#states' => array(
        'visible' => array(
          'input[name="list_type"]' => array(
            'value' => 'text',
          ),
        ),
      ),
    );
    
    $sites = array(
      'http://hidemyass.com/proxy-list/',
      'http://www.samair.ru/proxy/',
      'http://spys.ru/en/',
      'http://www.ip-adress.com/proxy_list/',
      'http://www.freeproxylists.net/ru/',
      'http://free-proxy-list.net/',
      'http://sockslist.net/',
      'http://proxy-share.com/',
      'http://proxy-list.org/ru/',
    );
    $sites_items = array_map(function ($url) {
      return '<a href="' . $url . '" target="_blank">' . $url . '</a>';
    }, $sites);
    $form['sites'] = array(
      '#type' => 'details',
      '#title' => 'Сайты со списками',
      '#collapsed' => TRUE,
      '#description' => theme('item_list', array('items' => $sites_items)),
      '#states' => array(
        'visible' => array(
          'input[name="list_type"]' => array(
            'value' => 'text',
          ),
        ),
      ),
    );
    
    $plugin_options = array();
    foreach ($this->cheatPluginManager->getDefinitions() as $definition) {
      $plugin_options[$definition['id']] = $definition['title'];
    }
    $form['plugin'] = array(
      '#type' => 'select',
      '#title' => 'Плагин',
      '#options' => $plugin_options,
      '#default_value' => isset($_SESSION['cheat']['plugin']) ? $_SESSION['cheat']['plugin'] : NULL,
    );
    
    $form['timeout'] = array(
      '#type' => 'textfield',
      '#title' => 'Задержка между удачными запросами',
      '#default_value' => isset($_SESSION['cheat']['timeout']) ? $_SESSION['cheat']['timeout'] : 30,
      '#field_suffix' => 'сек.',
      '#size' => 4,
    );
    
    $form['check_timeout'] = array(
      '#type' => 'textfield',
      '#title' => 'Максимальное время на проверку прокси',
      '#default_value' => isset($_SESSION['cheat']['check_timeout']) ? $_SESSION['cheat']['check_timeout'] : 20,
      '#field_suffix' => 'сек.',
      '#size' => 4,
    );
    
    $form['anonymity'] = array(
      '#type' => 'checkboxes',
      '#title' => 'Тип анонимности',
      '#options' => array(
        'transparent' => 'Transparent <acronym title="В HTTP_X_FORWARDED_FOR будет ваш IP">?</acronym></span>',
        'anonymous' => 'Anonymous <acronym title="В HTTP_VIA будет факт использования прокси">?</acronym></span>',
        'high_anonymous' => 'High Anonymous <acronym title="Полностью анонимный и без HTTP_VIA">?</acronym></span>',
      ),
      '#default_value' => isset($_SESSION['cheat']['anonymity']) ? $_SESSION['cheat']['anonymity'] : array(),
      '#required' => TRUE,
    );
    
    $form['pass_good'] = array(
      '#type' => 'checkbox',
      '#title' => 'Пропускать прокси, которые есть в списке проверенных и подходящих',
      '#default_value' => TRUE,
      '#states' => array(
        'visible' => array(
          'input[name="list_type"]' => array(
            'value' => 'text',
          ),
        ),
      ),
    );
    
    $form['clear_works'] = array(
      '#type' => 'checkbox',
      '#title' => 'Очистить список подходящих прокси',
      '#default_value' => FALSE,
    );
    
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Начать',
    );
    
    $module_path = drupal_get_path('module', 'cheat');
    $form['#attached']['js'][] = $module_path . '/misc/cheat.js';
    $form['#attached']['css'][] = $module_path . '/misc/cheat.css';
    
    $form['#suffix'] = '
      <div id="cheat-info">
        <div>Всего прокси:        <span class="cheat-info-proxy-count">0</span></div>
        <div>Прокси в очереди:    <span class="cheat-info-queue-count">0</span></div>
        <div>Использовано прокси: <span class="cheat-info-used-count">0</span></div>
        <div>Подходящих прокси:   <span class="cheat-info-executed-count">0</span></div>
        <div>Удачных выполнений:  <span class="cheat-info-true-count">0</span></div>
        <div>Время работы:        <span class="cheat-info-time">00:00:00</span></div>
        <div>Осталось времени:    <span class="cheat-info-time-remain">...</span></div>
      </div>
      <div id="cheat-log"></div>
    ';
    
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, array &$form_state) {
    // Empty...
  }
}
