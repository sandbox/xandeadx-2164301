<?php

namespace Drupal\cheat;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Component\Plugin\PluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Guzzle\Http\ClientInterface;
use Guzzle\Http\Exception\RequestException;

class CheatController extends ControllerBase {
  private $messages = array();
  
  /**
   * @var \Guzzle\Http\ClientInterface
   */
  protected $httpClient;
  
  /**
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $cheatPluginManager;
  
  /**
   * Controller constructor.
   */
  public function __construct(ClientInterface $http_client, PluginManagerInterface $cheat_plugin_manager) {
    $this->httpClient = $http_client;
    $this->cheatPluginManager = $cheat_plugin_manager;
  }
  
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_default_client'),
      $container->get('plugin.manager.cheat')
    );
  }
  
  /**
   * Prepare cheating.
   */
  public function prepare(Request $request) {
    $list_type = $request->request->get('list_type');
    $status = 1;
    $proxy_count = 0;
    
    // Save proxy list from request to file
    if ($list_type == 'text') {
      $proxy = $this->getProxyFromText($request->request->get('list'));
      
      if ($proxy) {
        if ($request->request->get('pass_good')) {
          $good_proxy = cheat_get_proxy_list('public://cheat-good-proxy.txt');
          $proxy = array_diff($proxy, $good_proxy);
        }
        
        $proxy_count = count($proxy);
      }
      else {
        $status = 0;
      }
      
      $this->saveProxyList($proxy, 'public://cheat-proxy.txt');
      $this->addMessage('Список сохранён. Найдено прокси: ' . count($proxy));
    }
    // Use previous proxy list
    elseif ($list_type == 'prev') {
      $proxy_count = count(cheat_get_proxy_list('public://cheat-proxy.txt'));
      $this->addMessage('Будет использован список из прошлого запуска. Число прокси: ' . $proxy_count);
    }
    // Save proxy list from good-list
    elseif ($list_type == 'work') {
      file_unmanaged_move('public://cheat-good-proxy.txt', 'public://cheat-proxy.txt', FILE_EXISTS_REPLACE);
      $proxy_count = count(cheat_get_proxy_list('public://cheat-proxy.txt'));
      $this->addMessage('Список сохранён из списка проверенных и подходящих прокси. Число прокси: ' . $proxy_count);
    }
    
    // Delete worked proxy
    if ($request->request->get('clear_works')) {
      file_unmanaged_delete('public://cheat-good-proxy.txt');
      $this->addMessage('Список подходящих прокси удалён.');
    }
    
    // Get current IP address
    $current_ip = $this->getCurrentIp();
    if ($current_ip) {
      $this->addMessage('Собственный IP адрес: ' . $current_ip);
    }
    else {
      $this->addMessage('Не удалось определить собственный IP адрес.');
      $status = 0;
    }
    
    // Check proxy type
    $anonymity = $request->request->get('anonymity');
    $anonymity = $anonymity ? array_keys($anonymity) : array();
    if (!$anonymity) {
      $this->addMessage('Не выбраны тип анонимности.');
      $status = 0;
    }
    
    // Save form state in session
    // @todo Wait https://drupal.org/node/1858196 or replace to service 'user.tempstore'
    $_SESSION['cheat']['list_type'] = $list_type;
    $_SESSION['cheat']['plugin'] = $request->request->get('plugin');
    $_SESSION['cheat']['timeout'] = $request->request->get('timeout');
    $_SESSION['cheat']['check_timeout'] = $request->request->get('check_timeout');
    $_SESSION['cheat']['anonymity'] = $anonymity;
    $_SESSION['cheat']['ip'] = $current_ip;
    
    return new JsonResponse(array(
      'status' => $status,
      'messages' => $this->getMessages(),
      'proxyCount' => $proxy_count,
    ));
  }
  
  /**
   * Run cheat iteration.
   */
  public function run() {
    $list = cheat_get_proxy_list('public://cheat-proxy.txt');
    $ip = trim(array_shift($list));
    $status = 1;
    $plugin_executed = FALSE;
    
    if ($_SESSION['cheat']['list_type'] == 'none') {
      $this->executePlugin($_SESSION['cheat']['plugin']);
      $status = 0;
    }
    elseif (!$ip) {
      $this->addMessage('Список прокси пуст.');
      $status = 0;
    }
    else {
      $this->addMessage('Проверяю прокси ' . $ip . '...');
      $proxy_info = $this->checkProxy($ip, $_SESSION['cheat']['ip'], $_SESSION['cheat']['check_timeout']);
      $proxy_text_status = $proxy_info
        ? '<span class="green">рабочий (' . $proxy_info['type'] . ', ' . $proxy_info['anonymity'] . ', ' . $proxy_info['country'] . ')</span>'
        : '<span class="red">не рабочий</span>';
      $this->addMessage('Прокси проверен: ' . $proxy_text_status);
      
      if ($proxy_info && in_array($proxy_info['anonymity'], $_SESSION['cheat']['anonymity'])) {
        // Execute plugin
        $plugin_result = $this->executePlugin($_SESSION['cheat']['plugin'], $proxy_info['address']);
        $plugin_executed = TRUE;
        
        // Add proxy to good-list
        $good_proxy = cheat_get_proxy_list('public://cheat-good-proxy.txt');
        $good_proxy[] = $ip;
        $this->saveProxyList($good_proxy, 'public://cheat-good-proxy.txt');
      }
      elseif ($proxy_info) {
        $this->addMessage('Параметры прокси не удовлетворяют настройкам.');
      }
      
      $this->saveProxyList($list, 'public://cheat-proxy.txt');
    }
    
    return new JsonResponse(array(
      'status' => $status,
      'messages' => $this->getMessages(),
      'pluginExecuted' => $plugin_executed,
      'pluginResult' => $plugin_executed ? $plugin_result : NULL,
      'proxyCount' => count($list),
    ));
  }
  
  /**
   * Request to URL and return response body.
   */
  public function simpleRequest($url, $proxy = NULL, $post_data = NULL, $user_agent = NULL, $referer = NULL, $headers = NULL, array $options = array()) {
    // Method
    $method = $post_data ? 'POST' : 'GET';
    
    // User agent
    if ($user_agent == 'random') {
      $user_agent = $this->getUserAgent(TRUE);
    }
    elseif (!$user_agent) {
      $user_agent = $this->getUserAgent();
    }
    $this->httpClient->setUserAgent($user_agent);
    
    // Proxy
    if ($proxy) {
      $options['proxy'] = $proxy;
    }
    
    // Referer
    if ($referer === NULL) {
      $referer = $url;
    }
    if (!isset($headers['Referer'])) {
      $headers['Referer'] = $referer;
    }
    
    // Default options
    $options += array('timeout' => 20);
    
    try {
      return $this->httpClient->createRequest($method, $url, $headers, $post_data, $options)->send()->getBody(TRUE);
    }
    catch (RequestException $e) {}
  }
  
  /**
   * Return User Agent.
   */
  public function getUserAgent($random = FALSE) {
    $rand_number = rand(10, 99);
    $user_agents = array(
      'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1150.63 Safari/527.36',
      'Mozilla/5.0 (Windows; Windows NT 5.1; es-ES; rv:1.9.2a1pre) Gecko/200904' . $rand_number . ' Firefox/3.6a1pre',
      'Mozilla/5.0 (Windows NT) AppleWebKit/534.20 (KHTML, like Gecko) Chrome/11.0.672.2 Safari/534.' . $rand_number,
      'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_6; en-US) AppleWebKit/534.20 (KHTML, like Gecko) Chrome/' . $rand_number . '.0.672.2 Safari/534.20',
      'Opera/9.80 (X11; Linux x86_64; U; Ubuntu/10.10 (maverick); pl) Presto/2.7.62 Version/11.' . $rand_number,
      'Opera/9.80 (Windows NT 5.1; U; cs) Presto/2.7.62 Version/11.' . $rand_number,
      'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.13) Gecko/20101213 Opera/9.80 (Windows NT 6.1; U; zh-tw) Presto/2.7.62 Version/11.' . $rand_number,
    );
    
    if ($random) {
      return $this->getRandomArrayValue($user_agents);
    }
    return current($user_agents);    
  }
  
  /**
   * Return random array value.
   */
  public function getRandomArrayValue($array) {
    return $array[array_rand($array)];
  }
  
  /**
   * Add message in response messages.
   */
  public function addMessage($mesage) {
    $this->messages[] = array(date('H:i:s'), $mesage);
  }
  
  /**
   * Return all messages.
   */
  protected function getMessages() {
    foreach (drupal_get_messages() as $messages) {
      foreach ($messages as $message) {
        $this->addMessage('Системное сообщение: ' . $message);
      }
    }
    return $this->messages;
  }
  
  /**
   * Return proxies from text.
   */
  protected function getProxyFromText($text) {
    if (preg_match_all('#(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})(?:\s+|:)(\d{2,5})#', $text, $matches, PREG_SET_ORDER)) {
      $list = array();
      foreach ($matches as $match) {
        $list[] = $match[1] . ':' . $match[2];
      }
      return array_unique($list);
    }
    return array();
  }
  
  /**
   * Return current IP address.
   */
  protected function getCurrentIp() {
    $response = $this->simpleRequest('http://forum.ru-board.com/e.pl');
    if (preg_match('#REMOTE_ADDR => (\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})#', $response, $matches)){
      return $matches[1];
    }
  }
  
  /**
   * Check proxy and if proxy worked then return proxy information.
   */
  protected function checkProxy($address, $current_ip, $timeout = 20) {
    $proxy_type = '';
    $proxy_address = '';
    $response = '';
    $rand_number = rand(1, 9999);
    $options = array('timeout' => $timeout);
    list($proxy_ip, $proxy_port) = explode(':', $address);
    
    // Check http proxy
    if ($proxy_port != 1080) {
      $proxy_address = $address;
      $proxy_type = 'http';
      $response = $this->simpleRequest('http://forum.ru-board.com/e.pl?r=' . $rand_number, $proxy_address, NULL, NULL, NULL, NULL, $options);
    }
    
    // Check socks proxy
    if (!$response) {
      $proxy_address = 'socks://' . $address;
      $proxy_type = 'socks';
      $response = $this->simpleRequest('http://forum.ru-board.com/e.pl?r=' . $rand_number, $proxy_address, NULL, NULL, NULL, NULL, $options);
    }
    
    if ($response && strpos($response, 'REMOTE_ADDR') !== false) {
      if (strpos($response, $current_ip) !== false) {
        $anonymity = 'transparent';
      }
      elseif (strpos($response, 'HTTP_VIA') !== false) {
        $anonymity = 'anonymous';
      }
      else {
        $anonymity = 'high_anonymous';
      }
      
      // IP info
      $ip_info = $this->getIpInfo($proxy_ip);
      
      return array(
        'type' => $proxy_type,
        'anonymity' => $anonymity,
        'address' => $proxy_address,
        'country' => $ip_info->geoplugin_countryName,
      );
    }
  }
  
  /**
   * Return IP info.
   */
  protected function getIpInfo($ip) {
    $result = $this->simpleRequest('http://www.geoplugin.net/json.gp?ip=' . $ip);
    return json_decode($result);
  }
  
  /**
   * Save proxy list.
   */
  protected function saveProxyList(array $list, $filename) {
    $list = array_unique($list);
    file_unmanaged_save_data(implode("\n", $list), $filename, FILE_EXISTS_REPLACE);
  }
  
  /**
   * Execute plugin.
   */
  protected function executePlugin($plugin_id, $proxy = '') {
    drupal_set_time_limit(0);
    $this->addMessage('Запускаю плагин ' . $plugin_id . '...');
    $cheat_plugin = $this->cheatPluginManager->createInstance($plugin_id);
    return $cheat_plugin->execute($proxy, $this);
  }
}
