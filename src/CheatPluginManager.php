<?php

namespace Drupal\cheat;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

class CheatPluginManager extends DefaultPluginManager {
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/Cheat', $namespaces);
    $this->alterInfo($module_handler, 'cheat_info');
    $this->setCacheBackend($cache_backend, 'cheat_plugins');
  }
}
