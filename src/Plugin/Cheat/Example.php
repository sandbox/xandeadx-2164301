<?php

namespace Drupal\cheat\Plugin\Cheat;

use Drupal\Component\Annotation\Plugin;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Utility\String;
use Drupal\Core\Annotation\Translation;
use Drupal\cheat\CheatController;

/**
 * @Plugin(
 *   id = "example",
 *   title = @Translation("Example")
 * )
 */
class Example extends PluginBase {
  public function execute($proxy, CheatController $controller) {
    $result = $controller->simpleRequest('http://example.com/vote/123', $proxy);
    $controller->addMessage('Ответ сервера: <span class="blue">' . String::checkPlain($result) . '</span>');
    return (strpos($result, 'Голос принят') !== FALSE);
  }
}
