(function ($, Drupal) {
  "use strict";

  Drupal.behaviors.cheat = {
    attach: function (context, settings) {
      $('#cheat-main-form').on('submit', function (event) {
        event.preventDefault();
        $('#edit-submit').attr('disabled', 'disabled');
        $('#cheat-info').show();
        $('#cheat-log').addClass('in-work').show();
        Drupal.cheat.showMessage('Начинаю работу...');
        Drupal.cheat.startTime = Drupal.cheat.getCurrentTime();

        $.post(Drupal.url('cheat/prepare'), $(this).serialize(), function (response) {
          if (Drupal.cheat.checkResponse(response)) {
            $('.cheat-info-proxy-count').html(response.proxyCount);
            $('.cheat-info-queue-count').html(response.proxyCount)
            Drupal.cheat.cheatRun();
          }
        });

        Drupal.cheat.intervalId = setInterval(function () {
          var timezoneOffset = (new Date()).getTimezoneOffset() * -1;
          var timeWork = Drupal.cheat.getCurrentTime() - Drupal.cheat.startTime;
          var formattedTime = Drupal.cheat.formatTime(timeWork - timezoneOffset*60*1000);
          $('.cheat-info-time').html(formattedTime);
        }, 1000);
      });
    }
  };

  Drupal.cheat = Drupal.cheat || {
    title: $('title').text(),
    
    /**
     * Check response.
     */
    checkResponse: function (response) {
      var returnValue = true;
      
      if (typeof response == 'string') {
        Drupal.cheat.showMessage('Ответ пришёл не в JSON: "' + response + '"');
        returnValue = false;
      }
      if (returnValue && response.status == 0) {
        returnValue = false;
      }
      if (returnValue && response.messages) {
        for (var i = 0; i < response.messages.length; i++) {
          Drupal.cheat.showMessage(response.messages[i][1], response.messages[i][0]);
        }
      }
      
      if (!returnValue) {
        Drupal.cheat.showMessage('<span class="message-finish">Завершено.</span>');
        $('#cheat-log').removeClass('in-work');
        clearInterval(Drupal.cheat.intervalId);
        
        var sound = new Audio(Drupal.url('modules/cheat/misc/ringout.wav'));
        sound.play();
      }
      
      return returnValue;
    },
    
    /**
     * Show message in log.
     */
    showMessage: function (message, date) {
      if (!date) {
        date = Drupal.cheat.formatTime(Drupal.cheat.getCurrentTime());
      }
      $('\
        <div class="message">\
          <span class="message-date">' + date + '</span>\
          <span class="message-text">' + message + '</span>\
        </div>\
      ').appendTo('#cheat-log');
      
      window.scrollBy(0, 9999);
    },
    
    /**
     * Return current time in ms
     */
    getCurrentTime: function () {
      return (new Date()).getTime();
    },
    
    /**
     * Return current time.
     */
    formatTime: function (time) {
      var dateObject = new Date(time);
      var hours = dateObject.getHours();
      var minutes = dateObject.getMinutes();
      var seconds = dateObject.getSeconds();
      return Drupal.cheat.getNumberWithZeros(hours, 2) + ':' +
        Drupal.cheat.getNumberWithZeros(minutes, 2) + ':' + 
        Drupal.cheat.getNumberWithZeros(seconds, 2);
    },
    
    /**
     * Return number with leading zeros.
     */
    getNumberWithZeros: function (number, length) {
      var numberLength = number.toString().length;
      if (numberLength < length) {
        number = new Array(length - numberLength + 1).join('0') + number;
      }
      return number;
    },
    
    /**
     * Run cheat iteration.
     */
    cheatRun: function () {
      Drupal.cheat.showMessage('<hr />');
          
      $.post(Drupal.url('cheat/run'), function (response) {
        if (Drupal.cheat.checkResponse(response)) {
          var timeout = 0;
          var timeoutSec = parseInt($('#edit-timeout').val());
          
          if (timeoutSec && response.pluginExecuted && response.pluginResult !== false && response.proxyCount > 0) {
            timeout = timeoutSec * 1000;
            Drupal.cheat.showMessage('Таймаут на ' + timeoutSec + ' сек.');
          }
    
          setTimeout(function () {
            Drupal.cheat.cheatRun();
          }, timeout);
          
          var usedCount = parseInt($('.cheat-info-used-count').html());
          $('.cheat-info-used-count').html(usedCount + 1);
          
          if (response.pluginExecuted) {
            var executedCount = parseInt($('.cheat-info-executed-count').html());
            $('.cheat-info-executed-count').html(executedCount + 1);
          }
          
          if (response.pluginResult) {
            var trueCount = parseInt($('.cheat-info-true-count').html());
            $('.cheat-info-true-count').html(trueCount + 1);
          }
          
          $('.cheat-info-queue-count').html(response.proxyCount);
          document.title = '(' + response.proxyCount + ') ' + Drupal.cheat.title;
          
          var timeWork = Drupal.cheat.getCurrentTime() - Drupal.cheat.startTime;
          var timezoneOffset = (new Date()).getTimezoneOffset() * -1;
          var usedCount = parseInt($('.cheat-info-used-count').html());
          var queueCount = parseInt($('.cheat-info-queue-count').html());
          var timeRemain = (queueCount / usedCount) * timeWork;
          var formattedTimeRemain = Drupal.cheat.formatTime(timeRemain - timezoneOffset*60*1000);
          $('.cheat-info-time-remain').html(formattedTimeRemain);
        }
      });
    }
  }
})(jQuery, Drupal);
